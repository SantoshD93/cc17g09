STACK="$1"

# Create the stack using server-landscape.yaml and defining all necessary parameters
open racoon.gif
openstack stack create -t server-landscape.yaml --parameter key_pair=grpkey --parameter external_net=tu-internal --parameter flavor='Cloud Computing' --parameter image=ubuntu-16.04 --parameter zone='Cloud Computing' --wait ${STACK}
